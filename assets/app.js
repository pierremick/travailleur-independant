/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';
import './vendor/hs-mega-menu/dist/hs-mega-menu.min.css';
import './vendor/bootstrap-icons/font/bootstrap-icons.css';

// start the Stimulus application
import './bootstrap';

const $ = require('jquery');
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
require('bootstrap');

// or you can include specific pieces
// require('bootstrap/js/dist/tooltip');
// require('bootstrap/js/dist/popover');

import './vendor/hs-header/dist/hs-header.min.js';

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});
