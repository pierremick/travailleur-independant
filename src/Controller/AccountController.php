<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/mon-compte')]
class AccountController extends AbstractController
{
    #[Route('/', name: 'account')]
    public function account(): Response
    {
        return $this->render('account/index.html.twig', [
            'page_name' => 'Mon compte',
            'controller_name' => 'AccountController',
        ]);
    }
}
