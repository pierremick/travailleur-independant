<?php

namespace App\Controller\Admin;

use App\Entity\Activity;
use App\Form\ActivityType;
use App\Repository\ActivityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\String\UnicodeString;

#[Route('/admin/activity')]
class ActivityController extends AbstractController
{
    #[Route('/', name: 'admin_activity_index', methods: ['GET'])]
    public function admin_activity_index(ActivityRepository $activityRepository): Response
    {
      $activities = $activityRepository->findBy([], ['name' => 'ASC']);

      return $this->render('admin/activity/index.html.twig', [
          'page_name' => 'Activités',
          'activities' => $activities,
      ]);
    }

    #[Route('/new', name: 'admin_activity_new', methods: ['GET', 'POST'])]
    public function admin_activity_new(Request $request, ActivityRepository $activityRepository, SluggerInterface $slugger): Response
    {
        $activity = new Activity();
        $form = $this->createForm(ActivityType::class, $activity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $activityName = (new UnicodeString($activity->getName()))->lower();
            $activityName = str_replace('&', '-', $activityName);
            $activity->setSlug($slugger->slug((string)$activityName));
            $activity->setCreatedAt(new \DateTimeImmutable());

            $activityRepository->save($activity, true);

            return $this->redirectToRoute('admin_activity_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/activity/new.html.twig', [
            'page_name' => 'Nouvelle activité',
            'activity' => $activity,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'admin_activity_show', methods: ['GET'])]
    public function admin_activity_show(Activity $activity): Response
    {
        return $this->render('admin/activity/show.html.twig', [
            'page_name' => $activity->getName(),
            'activity' => $activity,
        ]);
    }

    #[Route('/{id}/edit', name: 'admin_activity_edit', methods: ['GET', 'POST'])]
    public function admin_activity_edit(Request $request, Activity $activity, ActivityRepository $activityRepository, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(ActivityType::class, $activity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $activityName = (new UnicodeString($activity->getName()))->lower();
            $activityName = str_replace('&', '-', $activityName);
            $activity->setSlug($slugger->slug((string)$activityName));
            $activity->setUpdatedAt(new \DateTimeImmutable());
            $activityRepository->save($activity, true);

            return $this->redirectToRoute('admin_activity_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/activity/edit.html.twig', [
            'page_name' => 'Éditer l\'activité',
            'activity' => $activity,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'admin_activity_delete', methods: ['POST'])]
    public function admin_activity_delete(Request $request, Activity $activity, ActivityRepository $activityRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$activity->getId(), $request->request->get('_token'))) {
            $activityRepository->remove($activity, true);
        }

        return $this->redirectToRoute('admin_activity_index', [], Response::HTTP_SEE_OTHER);
    }
}
