<?php

namespace App\Controller\Admin;

use App\Entity\Job;
use App\Form\JobType;
use App\Repository\JobRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\String\UnicodeString;

#[Route('/admin/job')]
class JobController extends AbstractController
{
    #[Route('/', name: 'admin_job_index', methods: ['GET'])]
    public function admin_job_index(JobRepository $jobRepository): Response
    {
        $jobs = $jobRepository->findBy([], ['name' => 'ASC']);

        return $this->render('admin/job/index.html.twig', [
            'page_name' => 'Métiers',
            'jobs' =>  $jobs,
        ]);
    }

    #[Route('/new', name: 'admin_job_new', methods: ['GET', 'POST'])]
    public function admin_job_new(Request $request, JobRepository $jobRepository, SluggerInterface $slugger): Response
    {
        $job = new Job();
        $form = $this->createForm(JobType::class, $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $jobName = (new UnicodeString($job->getName()))->lower();
            $jobName = str_replace('&', '-', $jobName);
            $job->setSlug($slugger->slug((string)$jobName));
            $job->setCreatedAt(new \DateTimeImmutable());

            $jobRepository->save($job, true);

            return $this->redirectToRoute('admin_job_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/job/new.html.twig', [
            'page_name' => 'Nouveau métier',
            'job' => $job,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'admin_job_show', methods: ['GET'])]
    public function admin_job_show(Job $job): Response
    {
        return $this->render('admin/job/show.html.twig', [
            'page_name' => $job->getName(),
            'job' => $job,
        ]);
    }

    #[Route('/{id}/edit', name: 'admin_job_edit', methods: ['GET', 'POST'])]
    public function admin_job_edit(Request $request, Job $job, JobRepository $jobRepository, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(JobType::class, $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $jobName = (new UnicodeString($job->getName()))->lower();
            $jobName = str_replace('&', '-', $jobName);
            $job->setSlug($slugger->slug((string)$jobName));
            $job->setUpdatedAt(new \DateTimeImmutable());

            $jobRepository->save($job, true);

            return $this->redirectToRoute('admin_job_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/job/edit.html.twig', [
            'page_name' => 'Éditer le métier',
            'job' => $job,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'admin_job_delete', methods: ['POST'])]
    public function admin_job_delete(Request $request, Job $job, JobRepository $jobRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$job->getId(), $request->request->get('_token'))) {
            $jobRepository->remove($job, true);
        }

        return $this->redirectToRoute('admin_job_index', [], Response::HTTP_SEE_OTHER);
    }
}
