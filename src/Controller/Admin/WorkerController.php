<?php

namespace App\Controller\Admin;

use App\Entity\Worker;
use App\Form\WorkerType;
use App\Repository\WorkerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\String\UnicodeString;

#[Route('/admin/worker')]
class WorkerController extends AbstractController
{
    #[Route('/', name: 'admin_worker_index', methods: ['GET'])]
    public function admin_worker_index(WorkerRepository $workerRepository): Response
    {
        return $this->render('admin/worker/index.html.twig', [
            'page_name' => 'Freelances',
            'workers' => $workerRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'admin_worker_new', methods: ['GET', 'POST'])]
    public function admin_worker_new(Request $request, WorkerRepository $workerRepository, SluggerInterface $slugger): Response
    {
        $worker = new Worker();
        $form = $this->createForm(WorkerType::class, $worker);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $workerName = (new UnicodeString($worker->getName()))->lower();
            $workerName = str_replace('&', '-', $workerName);
            $worker->setSlug($slugger->slug((string)$workerName));
            $worker->setCreatedAt(new \DateTimeImmutable());

            // Enregistrer le worker pour obtenir son ID
            $workerRepository->save($worker, true);

            // Récupérer l'ID du worker
            $workerId = $worker->getId();

            // Modifier le slug avec l'ID
            $slugWithId = $worker->getSlug() . '-' . $workerId;
            $worker->setSlug($slugWithId);
            $worker->setUpdatedAt(new \DateTimeImmutable());

            // Enregistrer la modification du slug
            $workerRepository->save($worker, true);

            return $this->redirectToRoute('admin_worker_index', [], Response::HTTP_SEE_OTHER);
        }
        return $this->renderForm('admin/worker/new.html.twig', [
            'page_name' => 'Nouveau freelance',
            'worker' => $worker,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'admin_worker_show', methods: ['GET'])]
    public function admin_worker_show(Worker $worker): Response
    {
        return $this->render('admin/worker/show.html.twig', [
            'page_name' => $worker->getName(),
            'worker' => $worker,
        ]);
    }

    #[Route('/{id}/edit', name: 'admin_worker_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Worker $worker, WorkerRepository $workerRepository, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(WorkerType::class, $worker);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $workerName = (new UnicodeString($worker->getName()))->lower();
            $workerName = str_replace('&', '-', $workerName);
            $worker->setSlug($slugger->slug((string)$workerName));
            $worker->setUpdatedAt(new \DateTimeImmutable());
            // Récupérer l'ID du worker
            $workerId = $worker->getId();
            // Modifier le slug avec l'ID
            $slugWithId = $worker->getSlug() . '-' . $workerId;
            $worker->setSlug($slugWithId);
            // Enregistrer la modification du slug
            $workerRepository->save($worker, true);

            return $this->redirectToRoute('admin_worker_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/worker/edit.html.twig', [
            'page_name' => 'Éditer ' . $worker->getName(),
            'worker' => $worker,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'admin_worker_delete', methods: ['POST'])]
    public function admin_worker_delete(Request $request, Worker $worker, WorkerRepository $workerRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$worker->getId(), $request->request->get('_token'))) {
            $workerRepository->remove($worker, true);
        }

        return $this->redirectToRoute('admin_worker_index', [], Response::HTTP_SEE_OTHER);
    }
}
