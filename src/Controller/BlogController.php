<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Post;
use App\Entity\Tag;
use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('actualites')]
class BlogController extends AbstractController
{
    #[Route('/', name: 'actualites')]
    public function actualite(PostRepository $postRepository): Response
    {
        return $this->render('front/actualites.html.twig', [
            'page_name' => 'Actualités',
            'page_description' => 'Actualités',
            'posts' => $postRepository->findAll(),
        ]);
    }

    #[Route('/{slug}', name: 'article', methods: ['GET'])]
    public function article(Post $post): Response
    {
        return $this->render('front/article.html.twig', [
            'post' => $post,
            'page_name' => $post->getTitle(),
            'page_description' => $post->getDescription(),
        ]);
    }
}
