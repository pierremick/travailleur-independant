<?php

namespace App\Controller;

use App\Entity\Activity;
use App\Entity\Worker;
use App\Form\SearchFormType;
use App\Repository\ActivityRepository;
use App\Repository\WorkerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController
{
    #[Route('/', name: 'homepage')]
    public function homepage(): Response
    {
        return $this->render('front/homepage.html.twig', [
            'page_name' => 'Travailleur Indépendant',
            'page_description' => 'Vous êtes travailleur indépendant ? Bénéficiez des protections du salariat ✓… Oubliez les démarches administratives ✓… Soyez accompagnés.',
        ]);
    }

    #[Route('/freelance', name: 'freelance_index', methods: ['GET', 'POST'])]
    public function freelance_index(Request $request, WorkerRepository $workerRepository): Response
    {
        $form = $this->createForm(SearchFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $criteria = $form->getData();
            $workers = $workerRepository->findBySearchCriteria($criteria);
        } else {
            $workers = $workerRepository->findAllOpt();
        }

        // Pagination
        $page = $request->query->getInt('page', 1);
        $perPage = 6; // Nombre d'éléments par page

        $totalWorkers = count($workers);
        $totalPages = ceil($totalWorkers / $perPage);

        $prevPage = ($page > 1) ? $page - 1 : null;
        $nextPage = ($page < $totalPages) ? $page + 1 : null;

        $offset = ($page - 1) * $perPage;
        $paginatedWorkers = array_slice($workers, $offset, $perPage);

        return $this->render('front/freelance_index.html.twig', [
            'page_name' => 'Trouvez un freelance près de chez vous',
            'page_description' => 'meta description',
            'workers' => $paginatedWorkers,
            'searchForm' => $form->createView(),
            'workerRepository' => $workerRepository,
            'totalWorkers' => $totalWorkers,
            'totalPages' => $totalPages,
            'currentPage' => $page,
            'prevPage' => $prevPage,
            'nextPage' => $nextPage,
        ]);
    }

    #[Route('/freelance/{slug}', name: 'freelance_show', methods: ['GET'])]
    public function freelance_show(Worker $worker): Response
    {
        return $this->render('front/freelance_show.html.twig', [
            'page_name' => $worker->getName(),
            'worker' => $worker,
        ]);
    }

    #[Route('/activite', name: 'activity_index', methods: ['GET'])]
    public function activity_index(ActivityRepository $activityRepository): Response
    {
      $activities = $activityRepository->findBy([], ['name' => 'ASC']);

      return $this->render('front/activity_index.html.twig', [
          'page_name' => 'Activités',
          'activities' => $activities,
      ]);
    }

    #[Route('/activite/{slug}', name: 'activity_show', methods: ['GET'])]
    public function activity_show(Activity $activity): Response
    {
        return $this->render('front/activity_show.html.twig', [
            'page_name' => $activity->getName(),
            'activity' => $activity,
        ]);
    }
}
