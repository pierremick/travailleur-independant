<?php

namespace App\Entity;

use App\Repository\QuestionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: QuestionRepository::class)]
class Question
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 65, unique: true)]
    private ?string $title = null;

    #[ORM\Column(length: 65, unique: true)]
    private ?string $slug = null;

    #[ORM\Column(length: 255)]
    private ?string $answerShort = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $answerLong = null;

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getAnswerShort(): ?string
    {
        return $this->answerShort;
    }

    public function setAnswerShort(string $answerShort): self
    {
        $this->answerShort = $answerShort;

        return $this;
    }

    public function getAnswerLong(): ?string
    {
        return $this->answerLong;
    }

    public function setAnswerLong(?string $answerLong): self
    {
        $this->answerLong = $answerLong;

        return $this;
    }
}
