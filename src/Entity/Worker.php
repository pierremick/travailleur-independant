<?php

namespace App\Entity;

use App\Repository\WorkerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WorkerRepository::class)]
class Worker
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'workers')]
    private ?User $owner = null;

    #[ORM\ManyToOne(inversedBy: 'workers')]
    private ?Activity $activity = null;

    #[ORM\ManyToOne(inversedBy: 'workers')]
    private ?Job $job = null;

    #[ORM\ManyToMany(targetEntity: Skill::class, inversedBy: 'workers')]
    private Collection $skills;

    #[ORM\Column(length: 65)]
    private ?string $name = null;

    #[ORM\Column(length: 145, nullable: true)]
    private ?string $metaDescription = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $contentDescription = null;

    #[ORM\Column(length: 75, unique: true)]
    private ?string $slug = null;

    #[ORM\Column(nullable: true)]
    private ?bool $sap = null;

    #[ORM\Column(nullable: true)]
    private ?bool $itinerant = null;

    #[ORM\Column(nullable: true)]
    private ?bool $mobility = null;

    #[ORM\Column(nullable: true)]
    private ?bool $telework = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $socialFacebook = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $socialInstagram = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $socialGoogle = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $socialLinkedin = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $socialPinterest = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $socialTwitter = null;

    #[ORM\Column(length: 18, nullable: true)]
    private ?string $contactPhone = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $contactEmail = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $contactWebsite = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $address = null;

    #[ORM\Column(length: 5, nullable: true)]
    private ?string $addressZipcode = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $addressCity = null;

    #[ORM\Column(nullable: true, type: "float", scale: 7)]
    private ?float $lat = null;

    #[ORM\Column(nullable: true, type: "float", scale: 7)]
    private ?float $lng = null;

    public function __construct()
    {
        $this->skills = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getActivity(): ?Activity
    {
        return $this->activity;
    }

    public function setActivity(?Activity $activity): self
    {
        $this->activity = $activity;

        return $this;
    }

    public function getJob(): ?Job
    {
        return $this->job;
    }

    public function setJob(?Job $job): self
    {
        $this->job = $job;

        return $this;
    }

    /**
     * @return Collection<int, Skill>
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function addSkill(Skill $skill): self
    {
        if (!$this->skills->contains($skill)) {
            $this->skills->add($skill);
        }

        return $this;
    }

    public function removeSkill(Skill $skill): self
    {
        $this->skills->removeElement($skill);

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getContentDescription(): ?string
    {
        return $this->contentDescription;
    }

    public function setContentDescription(?string $contentDescription): self
    {
        $this->contentDescription = $contentDescription;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function isSap(): ?bool
    {
        return $this->sap;
    }

    public function setSap(?bool $sap): self
    {
        $this->sap = $sap;

        return $this;
    }

    public function isItinerant(): ?bool
    {
        return $this->itinerant;
    }

    public function setItinerant(?bool $itinerant): self
    {
        $this->itinerant = $itinerant;

        return $this;
    }

    public function isMobility(): ?bool
    {
        return $this->mobility;
    }

    public function setMobility(?bool $mobility): self
    {
        $this->mobility = $mobility;

        return $this;
    }

    public function isTelework(): ?bool
    {
        return $this->telework;
    }

    public function setTelework(?bool $telework): self
    {
        $this->telework = $telework;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getSocialFacebook(): ?string
    {
        return $this->socialFacebook;
    }

    public function setSocialFacebook(?string $socialFacebook): self
    {
        $this->socialFacebook = $socialFacebook;

        return $this;
    }

    public function getSocialInstagram(): ?string
    {
        return $this->socialInstagram;
    }

    public function setSocialInstagram(?string $socialInstagram): self
    {
        $this->socialInstagram = $socialInstagram;

        return $this;
    }

    public function getSocialGoogle(): ?string
    {
        return $this->socialGoogle;
    }

    public function setSocialGoogle(?string $socialGoogle): self
    {
        $this->socialGoogle = $socialGoogle;

        return $this;
    }

    public function getSocialLinkedin(): ?string
    {
        return $this->socialLinkedin;
    }

    public function setSocialLinkedin(?string $socialLinkedin): self
    {
        $this->socialLinkedin = $socialLinkedin;

        return $this;
    }

    public function getSocialPinterest(): ?string
    {
        return $this->socialPinterest;
    }

    public function setSocialPinterest(?string $socialPinterest): self
    {
        $this->socialPinterest = $socialPinterest;

        return $this;
    }

    public function getSocialTwitter(): ?string
    {
        return $this->socialTwitter;
    }

    public function setSocialTwitter(?string $socialTwitter): self
    {
        $this->socialTwitter = $socialTwitter;

        return $this;
    }

    public function getContactPhone(): ?string
    {
        return $this->contactPhone;
    }

    public function setContactPhone(?string $contactPhone): self
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(?string $contactEmail): self
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    public function getContactWebsite(): ?string
    {
        return $this->contactWebsite;
    }

    public function setContactWebsite(?string $contactWebsite): self
    {
        $this->contactWebsite = $contactWebsite;

        return $this;
    }

    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function setLat(?float $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng(): ?float
    {
        return $this->lng;
    }

    public function setLng(?float $lng): self
    {
        $this->lng = $lng;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAddressZipcode(): ?string
    {
        return $this->addressZipcode;
    }

    public function setAddressZipcode(?string $addressZipcode): self
    {
        $this->addressZipcode = $addressZipcode;

        return $this;
    }

    public function getAddressCity(): ?string
    {
        return $this->addressCity;
    }

    public function setAddressCity(?string $addressCity): self
    {
        $this->addressCity = $addressCity;

        return $this;
    }
}
