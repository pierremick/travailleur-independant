<?php

namespace App\EventListener;

use App\Repository\ActivityRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Twig\Environment;
class MenuListener
{
    private $activityRepository;
    private $twig;

    public function __construct(ActivityRepository $activityRepository, Environment $twig)
    {
        $this->activityRepository = $activityRepository;
        $this->twig = $twig;
    }

    public function onKernelResponse(ResponseEvent $event)
    {
        $response = $event->getResponse();
        $content = $response->getContent();

        $activities = $this->activityRepository->findAll();

        $menuContent = $this->twig->render('_partials/_front_header.html.twig', [
            'activities' => $activities,
        ]);

        // Injecter le contenu du menu dans la réponse
        $content = str_replace('<!-- Insert Menu Here -->', $menuContent, $content);

        $response->setContent($content);
    }
}
