<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SearchFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('jobTitle', TextType::class, [
                'required' => false,
                'label' => 'Activité, métier, compétence',
                'attr' => [
                    'id' => 'jobTitleForm',
                    'placeholder' => 'Activité, métier, compétence',
                    'aria-label' => 'Activité, métier, compétence',
                ],
                'label_attr' => [
                    'for' => 'jobTitleForm',
                    'class' => 'visually-hidden',
                ],
            ])
            ->add('city', TextType::class, [
                'required' => false,
                'label' => 'Ville ou code postal',
                'attr' => [
                    'id' => 'cityForm',
                    'placeholder' => 'Ville ou code postal',
                    'aria-label' => 'Ville ou code postal',
                ],
                'label_attr' => [
                    'for' => 'cityForm',
                    'class' => 'visually-hidden',
                ],
            ])
        ;
    }
}
