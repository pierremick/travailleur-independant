<?php

namespace App\Form;

use App\Entity\Activity;
use App\Entity\User;
use App\Entity\Worker;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;

class WorkerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('owner', EntityType::class, [
                'class' => User::class,
                'required' => true,
                'label' => 'Utilisateur',
                'choice_label' => 'fullname',
                'placeholder' => 'Choisissez un utilisateur...',
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ])
            ->add('activity', EntityType::class, [
                'required' => true,
                'label' => 'Activité',
                'class' => Activity::class,
                'choice_label' => 'name',
                'placeholder' => 'Choisissez une activité...',
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
            ])
            ->add('name', TextType::class, [
                'required' => true,
                'label' => 'Nom du freelance',
                'attr' => [
                    'placeholder' => 'Donnez un nom au freelance',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
            ])
            ->add('metaDescription', TextareaType::class, [
                'required' => false,
                'label' => 'Méta description',
                'help' => 'La méta description n\'est pas visible sur la page mais elle est importante pour donner des indications au moteurs de recherches (elle est limitée à 145 caractères).',
                'attr' => [
                    'placeholder' => 'Méta description',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
                'constraints' => [
                    new Length([
                        'max' => 145,
                        'maxMessage' => 'La méta description ne doit pas dépasser {{ limit }} caractères.',
                    ]),
                ],
            ])
            ->add('contentDescription', CKEditorType::class, [
                'required' => false,
                'label' => 'Description principale',
                'row_attr' => [
                    'class' => 'mb-3',
                ],
                'config' => [
                    'toolbar' => [
                        ['Bold', 'Italic', 'Underline'],
                        ['NumberedList', 'BulletedList'],
                        ['Link', 'Unlink'],
                    ],
                ],
            ])
            ->add('sap', CheckboxType::class, [
                'required' => false,
                'label' => 'S.A.P.',
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
            ])
            ->add('itinerant', CheckboxType::class, [
                'required' => false,
                'label' => 'Itinérant',
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
            ])
            ->add('mobility', CheckboxType::class, [
                'required' => false,
                'label' => 'Mobile',
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
            ])
            ->add('telework', CheckboxType::class, [
                'required' => false,
                'label' => 'Télé travail',
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
            ])
            ->add('contactPhone', TelType::class, [
                'required' => false,
                'label' => 'N° de téléphone',
                'attr' => [
                    'placeholder' => 'N° de téléphone',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
                'help' => 'Le numéro de téléphone doit être au format international.',
                'constraints' => [
                    new Regex([
                        'pattern' => '/^\+\d{2,3}(\s\d){4,5}$|^\+\d{11,12}$/',
                        'message' => 'Le format du numéro de téléphone est invalide.',
                    ]),
                    new Length([
                        'max' => 18,
                        'maxMessage' => 'Le numéro de téléphone ne doit pas dépasser {{ limit }} caractères.',
                    ]),
                ],
            ])
            ->add('contactEmail', EmailType::class, [
                'required' => false,
                'label' => 'Adresse e-mail',
                'attr' => [
                    'placeholder' => 'Adresse e-mail',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
            ])
            ->add('address', TextType::class, [
                'required' => false,
                'label' => 'Adresse',
                'attr' => [
                    'placeholder' => 'Adresse',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
            ])
            ->add('addressZipcode', NumberType::class, [
                'required' => false,
                'label' => 'Code postal',
                'attr' => [
                    'placeholder' => 'Code postal',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
                'constraints' => [
                    new Length([
                        'min' => 5,
                        'max' => 5,
                        'exactMessage' => 'Le code postal doit avoir exactement 5 chiffres.',
                    ]),
                    new Regex([
                        'pattern' => '/^\d{5}$/',
                        'message' => 'Le code postal ne peut contenir que des chiffres.',
                    ]),
                ],
            ])
            ->add('addressCity', TextType::class, [
                'required' => false,
                'label' => 'Ville',
                'attr' => [
                    'placeholder' => 'Ville',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^[A-Za-z\-\'\s]+$/',
                        'message' => 'La ville ne peut contenir que des lettres, le tiret -, le simple guillemet \' et l\'espace.',
                    ]),
                ],
            ])
            ->add('lat', NumberType::class, [
                'label' => 'Latitude',
                'required' => false,
                'scale' => 7,
                'attr' => [
                    'placeholder' => 'Latitude',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
            ])
            ->add('lng', NumberType::class, [
                'label' => 'Longitude',
                'required' => false,
                'scale' => 7,
                'attr' => [
                    'placeholder' => 'Longitude',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
            ])
            ->add('contactWebsite', UrlType::class, [
                'required' => false,
                'label' => 'Website',
                'attr' => [
                    'placeholder' => 'Url de votre site Internet',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
            ])
            ->add('socialFacebook', UrlType::class, [
                'required' => false,
                'label' => 'Facebook',
                'attr' => [
                    'placeholder' => 'Url de votre page Facebook',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
            ])
            ->add('socialGoogle', UrlType::class, [
                'required' => false,
                'label' => 'Google',
                'attr' => [
                    'placeholder' => 'Google',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(https?:\/\/).+$/',
                        'message' => 'Veuillez saisir une URL valide commençant par "http://" ou "https://".',
                    ]),
                ],
            ])
            ->add('socialInstagram', UrlType::class, [
                'required' => false,
                'label' => 'Instagram',
                'attr' => [
                    'placeholder' => 'Instagram',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(https?:\/\/).+$/',
                        'message' => 'Veuillez saisir une URL valide commençant par "http://" ou "https://".',
                    ]),
                ],
            ])
            ->add('socialLinkedin', UrlType::class, [
                'required' => false,
                'label' => 'LinkedIn',
                'attr' => [
                    'placeholder' => 'LinkedIn',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(https?:\/\/).+$/',
                        'message' => 'Veuillez saisir une URL valide commençant par "http://" ou "https://".',
                    ]),
                ],
            ])
            ->add('socialPinterest', UrlType::class, [
                'required' => false,
                'label' => 'Pinterest',
                'attr' => [
                    'placeholder' => 'Pinterest',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(https?:\/\/).+$/',
                        'message' => 'Veuillez saisir une URL valide commençant par "http://" ou "https://".',
                    ]),
                ],
            ])
            ->add('socialTwitter', UrlType::class, [
                'required' => false,
                'label' => 'twitter',
                'attr' => [
                    'placeholder' => 'Twitter',
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(https?:\/\/).+$/',
                        'message' => 'Veuillez saisir une URL valide commençant par "http://" ou "https://".',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Worker::class,
        ]);
    }
}
