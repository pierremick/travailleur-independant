<?php

namespace App\Repository;

use App\Entity\Worker;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Worker>
 *
 * @method Worker|null find($id, $lockMode = null, $lockVersion = null)
 * @method Worker|null findOneBy(array $criteria, array $orderBy = null)
 * @method Worker[]    findAll()
 * @method Worker[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Worker::class);
    }

    public function save(Worker $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Worker $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findAllOpt()
    {
        return $this->createQueryBuilder('w')
            ->leftJoin('w.owner', 'o')
            ->leftJoin('w.activity', 'a')
            ->leftJoin('w.job', 'j')
            ->leftJoin('w.skills', 's')
            ->getQuery()
            ->getResult();
    }

    public function findBySearchCriteria($criteria)
    {
        $query = $this->createQueryBuilder('w')
            ->leftJoin('w.activity', 'a')
            ->leftJoin('w.job', 'j')
            ->leftJoin('w.skills', 's');

        if ($criteria['jobTitle']) {
            $query->andWhere(
                $query->expr()->orX(
                    $query->expr()->like('a.name', ':jobTitle'),
                    $query->expr()->like('j.name', ':jobTitle'),
                    $query->expr()->like('s.name', ':jobTitle')
                )
            )->setParameter('jobTitle', '%' . $criteria['jobTitle'] . '%');
        }

        if ($criteria['city']) {
            $query->andWhere(
                $query->expr()->orX(
                    $query->expr()->like('w.addressCity', ':city'),
                    $query->expr()->like('w.addressZipcode', ':city')
                )
            )->setParameter('city', '%' . $criteria['city'] . '%');
        }

        return $query->getQuery()->getResult();
    }


//    /**
//     * @return Worker[] Returns an array of Worker objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('w')
//            ->andWhere('w.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('w.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Worker
//    {
//        return $this->createQueryBuilder('w')
//            ->andWhere('w.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
